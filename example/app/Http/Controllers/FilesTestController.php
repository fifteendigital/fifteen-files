<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Redirect;
use Exception;

use Fifteen\Files\Repositories\FilesRepository;

class FilesTestController extends Controller
{

    public function __construct(FilesRepository $repo)
    {
        $this->repo = $repo;
    }

    public function index()
    {
        $files = $this->repo->files('test');
        return view('files-test', compact('files'));
    }

    public function store(Request $request)
    {
        try {
            $file = $request->file('file');
            $this->storeFile($file, 'test/' . $file->getClientOriginalName());
            return Redirect::to('files-test')->withMessage("File uploaded!");
        } catch (Exception $e) {
            return Redirect::back()->withInput()->withMessage($e->getMessage());
        }
    }   

    public function delete($path)
    {
        try {
            $this->deleteFile($path);
            return Redirect::to('files-test')->withMessage("File deleted!");
        } catch (Exception $e) {
            return Redirect::back()->withInput()->withMessage($e->getMessage());
        }
    }

    public function storeFile(UploadedFile $file, $path) 
    {
        $this->repo->storeFile($path, file_get_contents($file->getRealPath()));
    }

    public function deleteFile($path)
    {
        $this->repo->deleteFile($path);
    }

}