<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function() {

    Route::get('/files/{path?}', [                  
            'as' => 'files', 
            'uses' => 'FilesController@getFile'
        ])->where('path', '(.*)');
    
    Route::get('/images/{path?}', [
            'as' => 'images', 
            'uses' => 'ImagesController@getFile'
        ])->where('path', '(.*)');

    Route::get('/thumb/{type}/{size}/{path?}', [
            'as' => 'thumbnails', 
            'uses' => 'ImagesController@getThumbnail'
        ])->where('path', '(.*)');

    Route::get('files-test', 'FilesTestController@index');
    Route::post('files-test', 'FilesTestController@store');
    Route::get('files-test/delete/{path?}', ['as' => 'files-test.delete', 'uses' => 'FilesTestController@delete'])->where('path', '(.*)');

});
