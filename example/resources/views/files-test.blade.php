<p>{{ Session::get('message') }}</p>

<form method="post" enctype="multipart/form-data">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <input name="file" type="file" />
    <input type="submit" />
</form>

@foreach ($files as $file)
<p>
    <a href="{{ route('files', $file) }}" target="_blank">
        {{ basename($file) }}
    </a>
    &ensp;
    <a href="{{ route('files-test.delete', $file) }}">
        x
    </a>
</p>
@endforeach