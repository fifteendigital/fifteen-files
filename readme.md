#Files

Serves up files, images and creates thumbnails on the fly. Integrates with flysystem and works with AWS S3.

##Installation

Add Fifteen/Files to your composer.json file:

    "require": {
        "php": ">=5.5.9",
        "laravel/framework": "5.1.*",
        "fifteen/files": "1.0.*"
    },

Add minimum-stability: dev (IMPORTANT!):
```sh
    "minimum-stability": "dev",
```

As the repository isn't on Packagist, you also need to include it in the repositories list:

```php
    "repositories": [
        {
            "type": "vcs",
            "url":  "git@bitbucket.org:fifteendigital/fifteen-files.git"
        }
    ]
```

Run composer update:

```sh
$ composer update
```

Register service provider in config/app.php:

```php
    'providers' => [

        /*
         * Laravel Framework Service Providers...
         */
        ...

        /*
         * Package Service Providers...
         */
        Fifteen\Files\FilesServiceProvider::class
    ];
```

Publish files to application:

```sh
$ php artisan vendor:publish --provider="Fifteen\Files\FilesServiceProvider" --force
```

##S3 integration

To use S3, you need to first install the AWS S3 driver

```sh
$ composer require league/flysystem-aws-s3-v3 ~1.0
```

Then add your S3 details to config/filesystems.php (remember to use environment variables for your keys!):

```sh
    'default' => 's3',
```

```sh
        's3' => [
            'driver' => 's3',
            'key'    => env('AWS_S3_KEY'),
            'secret' => env('AWS_S3_SECRET'),
            'region' => env('AWS_S3_REGION'),
            'bucket' => env('AWS_S3_BUCKET'),
        ],
```

##Viewing files and thumbnails

To use the inbuilt controllers to view files and thumbnails, you will need to create the following files in your App/Http/Controllers folder:

FilesController.php:
```php
<?php

namespace App\Http\Controllers;

use Fifteen\Files\Http\Controllers\FilesController as BaseController;

class FilesController extends BaseController
{

}
```

ImagesController.php:
```php
<?php

namespace App\Http\Controllers;

use Fifteen\Files\Http\Controllers\ImagesController as BaseController;

class ImagesController extends BaseController
{

}
```

Then add the following to your routes file:
```php
Route::group(['middleware' => ['web', 'auth']], function() {

    Route::get('/files/{path?}', [                  
            'as' => 'files', 
            'uses' => 'FilesController@getFile'
        ])->where('path', '(.*)');
    
    Route::get('/images/{path?}', [
            'as' => 'images', 
            'uses' => 'ImagesController@getFile'
        ])->where('path', '(.*)');

    Route::get('/thumb/{type}/{size}/{path?}', [
            'as' => 'thumbnails', 
            'uses' => 'ImagesController@getThumbnail'
        ])->where('path', '(.*)');
});
```

This adds three routes:

```sh
/files/{path?}
/images/{path?}
/thumb/{type}/{size}/{path?}
```

These can be referenced in your views as follows:
```sh
    <a href="{{ url('files/' . $file) }}">View file</a>

    <a href="{{ route('images', $image) }}" target="_blank">
        {{ basename($image) }}
    </a>

    <img src="{{ route('thumbnails', ['crop', '100x100', $image]) }}" alt="Image" />
```

The images path is essentially an alias for files, except that if not found, it can display an optional default image.
The thumbnails path can be used to reference an image in the file system, but the thumbnail type (crop/fit/stretch) and size (e.g. 100x120).
If only one dimension needs to be specified, e.g. specifying a height of 200, make the width 0, i.e. 0x200.

##Handling authentication

The routes snippet above makes the files accessible only when logged in (using the 'auth' middleware).
If you wish to make all files public, remove 'auth'.
If you need to implement additional processing (e.g. updating access logs) or specific access controls, you will need to ensure that the files are not accessible elsewhere, by using middleware or subclassing the controllers where appropriate.

##Add/update/delete files

Use the FilesRepository and ImagesRepository classes to update and delete files.

```php
    public function storeFile(UploadedFile $file, $path) 
    {
        $repo = \App::make('\Fifteen\Files\Repositories\FilesRepository');
        $repo->storeFile($path, file_get_contents($file->getRealPath()));
    }

    public function deleteFile($path)
    {
        $repo = \App::make('\Fifteen\Files\Repositories\FilesRepository');
        $repo->deleteFile($path);
    }
```

In addition, updating/deleting a file using the ImagesRepository class removes any temporary thumbnail files associated with it.

##More examples

A more complete example can be found in the example directory.
