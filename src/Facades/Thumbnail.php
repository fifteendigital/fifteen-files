<?php namespace Fifteen\Files\Facades;

use Illuminate\Support\Facades\Facade;

class Thumbnail extends Facade 
{
	protected static function getFacadeAccessor() 
	{ 
		return 'thumbnail'; 
	}
}