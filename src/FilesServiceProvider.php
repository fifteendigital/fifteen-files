<?php namespace Fifteen\Files;

use Illuminate\Support\ServiceProvider;

class FilesServiceProvider extends ServiceProvider {

	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = false;

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
	}
	
	/**
	 * Perform post-registration booting of services.
	 *
	 * @return void
	 */
	public function boot()
	{
		// Don't make routes automatic - must be implemented into your app's route file
	    // if (! $this->app->routesAreCached()) {
	    //     require __DIR__ . '/Http/routes.php';
	    // }
        if (! $this->app->routesAreCached()) {
            require_once __DIR__ . '/helpers.php';
        }
        $this->publish();
	}

	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return ['fifteen.files'];
	}

	private function publish()
    {
        $this->publishes([
            __DIR__ . '/../www/config/fifteen-files.php' => config_path('fifteen-files.php'),
        ], 'config'); 

    }

}