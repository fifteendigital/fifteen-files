<?php

namespace Fifteen\Files\Http\Controllers;

use League\Flysystem\FileNotFoundException;
use Illuminate\Routing\Controller as BaseController;

use Fifteen\Files\Repositories\FilesRepository as Repository;
use Redis;

use App;
use Config;
use Response;

class FilesController extends BaseController
{

    protected $repo;
    protected $cache;
    protected $enable_redis = false;

    public function __construct(Repository $repo)
    {
        $this->repo = $repo;
        if (isRedisEnabled()) {
            $this->cache = Redis::connection();
            // Redis::command('FLUSHALL');
            // dd(Redis::command('KEYS', ['*']));
            $this->enable_redis = Config::get('fifteen-files.redis.enabled');
        } else {
            $this->enable_redis = false;
        }
    }

    /**
    * If Redis is enabled, it keeps a record of files that have been saved, so that a direct URL (e.g. AWS S3) can be used
    **/
    public function saveToCache($filename)
    {
        if ($this->enable_redis) {
            $this->cache->set(getCacheKey($filename), 1);
        }
    }


    public function getFile($filename)
    {
        try {
            $file = $this->repo->readFile($filename);
            $mimetype = $this->repo->getMimeType($filename);
            $this->saveToCache($filename);
            return $this->respondWithContents($file, $mimetype);
        } catch (FileNotFoundException $e) {
            App::abort(404);
        }
    }

    public function respondWithContents($file, $mimetype, $status = 200)
    {
        $response = Response::make($file, $status);
        $response->header('Content-type', $mimetype);
        return $response;
    }

}
