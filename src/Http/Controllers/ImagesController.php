<?php

namespace Fifteen\Files\Http\Controllers;

use Fifteen\Files\Repositories\ImagesRepository as Repository;

use League\Flysystem\FileNotFoundException;

use Config;
use Response;

class ImagesController extends FilesController
{

    public function __construct(Repository $repo)
    {
        parent::__construct($repo);
    }

    public function parsePath($resource)
    {
        $extension = pathinfo($resource, PATHINFO_EXTENSION);
        $path = explode('/', substr($resource, 0, strripos($resource, '.' . $extension)));
        $size = array_pop($path);
        $type = array_pop($path);
        return [$type, $size, implode('/', $path) . '.' . $extension];
    }

    // public function getThumbnail($type, $size, $filename)
    public function getThumbnail($resource)
    {
        list($type, $size, $filename) = $this->parsePath($resource);
        try {

            $image = $this->repo->getThumbnail($type, $size, $filename, $mimetype);
            $this->saveToCache($this->repo->getThumbnailPath($type, $size, $filename));
            return $this->respondWithContents($image, $mimetype);
        } catch (FileNotFoundException $e) {
            // Show default image if none present
            return $this->getDefaultImage();
        }
    }

    public function getDefaultImage()
    {
        $image_unavailable_path = Config::get('fifteen-files.image_unavailable_path');
        if (!empty($image_unavailable_path)) {
            $path = base_path($image_unavailable_path);
            if (file_exists($path)) {
                $file = file_get_contents($path);
                $response = Response::make($file, 200);
                $response->header('Content-type', 'image/jpeg');
                return $response;
            }
        }
        return 'default image';
    }
}
