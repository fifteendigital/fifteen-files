<?php

/**
* Please note: these routes are not wired up by default; 
* you must add these to your app's route file, 
* taking care to use middleware or extend the controllers where appropriate.
**/

\Route::group(['namespace' => 'Fifteen\Files\Http\Controllers'], function() {

	\Route::get('/files/{path?}', 						'FilesController@getFile')->where('path', '(.*)');
	\Route::get('/images/{path?}', 						'ImagesController@getFile')->where('path', '(.*)');
	\Route::get('/thumb/{type}/{size}/{path?}', 		'ImagesController@getThumbnail')->where('path', '(.*)');

});