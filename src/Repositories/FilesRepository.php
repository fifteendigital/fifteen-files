<?php 

namespace Fifteen\Files\Repositories;

use Illuminate\Contracts\Filesystem\Filesystem;
use Exception;

class FilesRepository implements FilesRepositoryInterface
{
    protected $filesystem;

    public function __construct(Filesystem $filesystem)
    {
        $this->filesystem = $filesystem;
    }

    public function readFile($path)
    {
        return $this->filesystem->read($path);
    }

    public function getMimeType($path)
    {
        return $this->filesystem->mimeType($path);
    }

    public function hasFile($path)
    {
        $exists = $this->filesystem->exists($path);
        return $exists;
    }

    public function deleteFile($path)
    {
        if (isRedisEnabled()) {
            $keys = \Redis::keys(getCacheKey($path . '*'));
            foreach ($keys as $key) {
                \Redis::del($key);
            }
        }
        return $this->filesystem->delete($path);
    }

    public function saveStream($path, $source)
    {
        $stream = fopen($source, 'r+');
        return $this->filesystem->writeStream($path, $stream);
    }

    public function storeFile($path, $content)
    {
        return $this->filesystem->put($path, $content);
    }

    public function createDir($dir)
    {
        return $this->filesystem->makeDirectory($dir);
    }
    
    public function getMetaData($path)
    {
        return $this->filesystem->getMetaData($path);
    }

    public function getFullPath($path)
    {
        return storage_path('app/' . $path);
    }

    /**
    * Pass through any methods to filesystem
    **/
    public function __call($m, $a)
    {
        if (method_exists($this->filesystem, $m)) {
            return call_user_func_array([$this->filesystem, $m], $a);
        } else {
            throw new Exception('Method does not exist!');
        }
    }

}