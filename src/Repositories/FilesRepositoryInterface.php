<?php namespace Fifteen\Files\Repositories;

interface FilesRepositoryInterface
{
	public function readFile($path);

	public function getMimeType($path);
	
	public function hasFile($path);

	public function storeFile($path, $content);
}