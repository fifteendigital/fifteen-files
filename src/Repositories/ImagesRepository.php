<?php 

namespace Fifteen\Files\Repositories;

use Fifteen\Files\Repositories\FilesRepository;
use Fifteen\Files\Support\Thumbnail;

use League\Flysystem\FileNotFoundException;

class ImagesRepository extends FilesRepository
{

    public function getThumbnail($type, $size, $filename, &$mimetype = null)
    {
        $path = $this->getThumbnailPath($type, $size, $filename);
        // If thumbnail exists, return it
        if ($this->hasFile($path)) {
            $mimetype = $this->getMimeType($path);
            return $this->readFile($path);
        }
        // get original image
        $file = $this->readFile($filename);
        // create thumbnail, and scale according to type/size
        $thumb = new Thumbnail($file);
        $size = $this->decodeSize($size);
        $thumb->$type($size[0], $size[1]);
        // save for future use
        $this->storeFile($path, $thumb->outputThumbnail());
        // present newly created thumbnail image
        $mimetype = $thumb->getMimeType();
        return $thumb->outputThumbnail();
    }

    public function getThumbnailPath($type, $size, $filename)
    {
        $extension = pathinfo($filename, PATHINFO_EXTENSION);
        $filename = substr($filename, 0, strripos($filename, '.' . $extension));
        $path = 'thumb/' . $filename . '/' . $type . '/' . $size . '.' . $extension;
        return $path;
    }

    public function decodeSize($size)
    {
        if (stripos($size, 'x') != false) {
            return explode('x', $size);
        }
    }

    public function storeFile($path, $content)
    {
        $output = parent::storeFile($path, $content);
        $this->deleteThumbnails($path);
        return $output;
    }

    public function deleteFile($path)
    {
        $output = parent::deleteFile($path);
        $this->deleteThumbnails($path);
        return $output;
    }

    public function deleteThumbnails($path)
    {
        $extension = pathinfo($path, PATHINFO_EXTENSION);
        $filename = substr($path, 0, strripos($path, '.' . $extension));
        $path = 'thumb/' . $filename;
        if (isRedisEnabled()) {
            $keys = \Redis::keys(getCacheKey($path . '*'));
            foreach ($keys as $key) {
                \Redis::del($key);
            }
        }
        $this->filesystem->deleteDir($path);
    }

}