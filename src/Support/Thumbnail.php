<?php 

namespace Fifteen\Files\Support;

class Thumbnail
{

    protected $image;           // Image resource
    protected $info;
    protected $thumb;
    protected $width;
    protected $height;

    public function __construct($blob)
    {
        $this->image = imagecreatefromstring($blob);
        $this->thumb = $this->image;
        $this->info = getimagesizefromstring($blob);
        $this->width = $this->getWidth();
        $this->height = $this->getHeight();
    }

    public function getWidth()
    {
        return $this->info[0];
    }

    public function getHeight()
    {
        return $this->info[1];
    }

    public function getMimeType()
    {
        return $this->info['mime'];
    }

    public function crop($width, $height)
    {
        $origRatio = self::ratio($this->width, $this->height);
        self::adjustWidthHeightToRatio($width, $height, $origRatio);
        
        $newRatio = self::ratio($width, $height);
        $smallerDimension = self::smallerDimension($this->width, $this->height);
        // p($width, $height, $origRatio, $newRatio, $smallerDimension);
        if ($smallerDimension == 'width') {
            $origHeight = $this->width / $newRatio;
            $origWidth = $this->width;
        } else {
            $origWidth = $this->height * $newRatio;
            $origHeight = $this->height;
        }

        // TODO - the following code fixes an issue if the original dimension is greater than the image canvas
        // - perhaps the above should be optimized so it's not necessary?
        if ($origWidth > $this->width) {
            $origWidth = $this->width;
            $origHeight = $origWidth / $origRatio;
        } elseif ($origHeight > $this->height) {
            $origHeight = $this->height;
            $origWidth = $origHeight * $origRatio;
        }

        // pd($width, $height, $origWidth, $origHeight);
        return $this->resize($width, $height, $origWidth, $origHeight);
    }


    public function fit($width, $height)
    {
        $origRatio = self::ratio($this->width, $this->height);
        self::adjustWidthHeightToRatio($width, $height, $origRatio);

        $smallerDimension = self::smallerDimension($width, $height);
        // p($width, $height, $origRatio, $smallerDimension);

        if ($smallerDimension == 'width') {
            $height = $width / $origRatio;
        } else {
            $width = $height * $origRatio;
        }
        // pd($width, $height, $this->getWidth(), $this->getHeight());
        return $this->resize($width, $height, $this->getWidth(), $this->getHeight());
    }

    public function stretch($width, $height)
    {
        return $this->resize($width, $height, $this->getWidth(), $this->getHeight());
    }

    public function resize($width, $height, $origWidth, $origHeight)
    {
        $this->thumb = imagecreatetruecolor($width, $height);
        imagecopyresampled($this->thumb, $this->image, 0, 0, 0, 0, $width, $height, $origWidth, $origHeight);
    }

    public function outputThumbnail()
    {
        ob_start();
        switch($this->getMimeType()) {
            case 'image/jpeg':
                imagejpeg($this->thumb);
                break;
            case 'image/png':
                imagepng($this->thumb);
                break;
            case 'image/gif':
                imagegif($this->thumb);
                break;
        }
        $contents =  ob_get_contents();
        ob_end_clean();
        return $contents;
    }

    public static function smallerDimension($width = 0, $height = 0)
    {
        if ($width == $height) {
            return 'same';
        } else {
            return $width < $height ? 'width' : 'height';
        }
    }

    public static function ratio($width, $height)       // Ratio between width and height. 0: same; >0: wider; <0: taller
    {
        return $height == 0 ? 1 : $width / $height;
    }

    /**
    * If either the width or height are zero, adjust to ratio
    **/
    public static function adjustWidthHeightToRatio(&$width, &$height, $ratio)
    {
        if ($height == 0) {
            $height = $width / $ratio;
        } elseif ($width == 0) {
            $width = $height * $ratio;
        }
    }

}