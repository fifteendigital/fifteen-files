<?php

function getResourceLink($resource, $thumbType = null, $width = 0, $height = 0) {
	if (empty($thumbType) || (empty($width) && empty($height))) {
		return url('files/' . $resource);
	}
	$path = getThumbPath($resource, $thumbType, $width, $height);
	return url($path);
}

function getDirectResourceLink($resource, $thumbType = null, $width = 0, $height = 0) {
	$path = getThumbPath($resource, $thumbType, $width, $height);
	if (isS3Enabled() && isRedisEnabled()) {
		if (empty($thumbType) || \Redis::exists(getCacheKey($path))) {     // if it's not a thumbnail, just show direct link
			return getS3Url() . '/' . $path;
		}
	}
	// Just return standard resource link
	return getResourceLink($resource, $thumbType, $width, $height);
}


/**
* Get path to thumbnail file
* $thumbType - 'crop' or 'fit'
**/
function getThumbPath($resource, $thumbType = null, $width = 0, $height = 0) {
	if (empty($thumbType) || (empty($width) && empty($height))) {
		return $resource;
	}
	$sizeString = $width . 'x' . $height;
	$extension = pathinfo($resource, PATHINFO_EXTENSION);
	$filename = substr($resource, 0, strripos($resource, '.' . $extension));
	$path = 'thumb/' . $filename . '/' . $thumbType . '/' . $sizeString . '.' . $extension;
	return $path;
}

function getS3Url() {
	$region = \Config::get('filesystems.disks.s3.region');
	$bucket = \Config::get('filesystems.disks.s3.bucket');
	return 'https://s3-' . $region . '.amazonaws.com/' . $bucket;
}

function isS3Enabled() {
	return \Config::get('filesystems.default') == 's3';
}

function isRedisEnabled() {
    if (\Config::get('fifteen-files.redis.enabled')) {
        try {
            \Redis::exists('test');
        } catch (\Predis\Connection\ConnectionException $ex) {
            return;
        }
        return true;
    }
}

function getCacheKey($filename) {
    $redis_tag = \Config::get('fifteen-files.redis.tag');
    if (!empty($redis_tag)) {
        return $redis_tag . ':' . $filename;
    }
    return $filename;
}

// function getLocalFileUrl($resource, $thumbType = null, $width = 0 , $height = 0) {
// 	if (empty($thumbType) || (empty($width) && empty($height))) {
// 		return url('files/' . $path);
// 	}
// 	$sizeString = $width . 'x' . $height;
// 	return url('thumb/' . $thumbType . '/' . $sizeString . '/' . $path);
// }
// function getRemotePath($filename, $thumbType = null, $width = 0, $height = 0) {
// 	if (empty($thumbType) || (empty($width) && empty($height))) {
// 		return $filename;
// 	}
// 	$sizeString = $width . 'x' . $height;
// 	$extension = pathinfo($filename, PATHINFO_EXTENSION);
// 	$filename = substr($filename, 0, strripos($filename, '.' . $extension));
// 	$path = 'thumb/' . $filename . '/' . $thumbType . '/' . $sizeString . '.' . $extension;
// 	return $path;
// }
