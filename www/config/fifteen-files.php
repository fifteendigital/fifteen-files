<?php

return [
    'redis' => [
        'enabled' => env('REDIS_ENABLED', false),
        'tag' => env('REDIS_TAG', ''),
    ],
    'image_unavailable_path' => 'resources/assets/img/placeholder-image.jpg',
];
